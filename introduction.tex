
\section{Introduction}
\label{sec:intro}


\ak{

\textbf{Text in intro is from proposal, unedited.}


Persistent Memory (PM) refers to memory technologies which provide
low-latency byte-level loads and stores (similar to DRAM) with the
added advantage of retaining data after a power
loss~\cite{raoux2008phase, xue2011emerging, ho2009nonvolatile,
  strukov2008missing, chua2011resistance, kultursay2013evaluating,
  lee2009pcm, lai2003current, wong2010phase}. PM-based technologies
such as Intel's 3D XPoint are set to appear on the market
soon~\cite{3dxpoint}.  Along with low latency and persistence,
persistent memory also has the advantage of higher density and lower
power consumption compared to DRAM. Used correctly, PM can deliver
significant increase in performance for a range of
applications~\cite{pcm}.  However, there still remain significant
challenges to using persistent memory in an efficient manner; this
proposal focuses on solving one such critical problem.

\vheading{Research Objectives}. The primary goal of this proposal is
to improve the efficiency with which an application's dirty data is
written-back from the volatile cache hierarchy to PM. The write-back 
latency should only depend upon the
amount of dirty data; as we show later in this proposal, it currently
depends upon other factors like the operating-system page size. We
believe \emph{at least} an order-of-magnitude better performance
can be obtained compared to current state-of-the-art.

\vheading{Motivation}. Given the low latency of PM, a popular method
to access such memory is to memory map (\mmap) a region of the PM
into the address space of a process, and then directly access it using
loads and stores; each address in the memory-mapped region will
correspond to an address on PM. The problem with this approach is
that since the memory is directly accessed, software systems like
the file system cannot keep track of the dirty data that needs to be
flushed to persistent media on a subsequent \msync; keeping track of
dirty data has traditionally been a task of the file system or the
operating system. Currently, the operating system page tables keep
track of which pages are dirty in memory. Since the tracking is at the
granularity of the whole page, even if one byte is modified, the whole
page has to be written-back to persistent memory. Since the minimum
unit that can be written back to PM is 64 bytes (one cache line), the
overhead of writing back an entire page can be as much as 64\myx for a
page size of 4KB. Due to the high density of PMs~\cite{3dxpoint,pcm},
it is quite likely that the large amount of persistent memory would
require using huge pages~\cite{shanley1996pentium, alpha2014alpha,
  DBLP:conf/osdi/NavarroIDC02}; the overhead of flushing a 2 MB huge
page when you only needed to flush one cacheline is
\textbf{32768\myx}. Put simply, the cost of writing-back dirty data,
both today and in the near future, is so high as to render persistent
memory systems impractical. In this proposal, we bring together
expertise from systems and computer architecture to solve
this crucial problem.

\vheading{Preliminary Work}. We have measured the cost of
writing-back 64 bytes of data in different file system configurations.
The results are presented in detail in \ref{sec:preliminary}.
Using the POSIX interface (\syswrite and \fsync) leads to the highest
latency, while using Direct Access (DAX)~\cite{linux-dax,windowsdax} cuts the
latency of writing-back dirty data by almost 50\%. However, assuming we
knew which cachelines are dirty, writing-back just those cachelines leads
to an almost 20\myx reduction in latency. Our experiments clearly show
that if we achieve our goal of efficiently tracking dirty data,
significant increases in performance are possible. 

We put forth three solutions to this problem (spanning software and hardware),
each described in a research thrust. The solutions vary in the time it will
take to implement them, and how completely they solve the problem.
%We describe
%both software and hardware solutions.
%Figure~\ref{fig:research} provides an overview of our three solutions. We now
%provide a brief description of each solution.

%\begin{figure*}[!t]
%  \centering
%  \begin{tabular}{ccc}
%\subfloat[]{\label{fig:quill}\includegraphics[height=6cm,width=0.26\textwidth]{figures/quill_api.pdf}} &
%\subfloat[]{\label{fig:compilerext}\includegraphics[height=6cm,width=0.32\textwidth]{figures/compiler_extensions.pdf}}
%\subfloat[]{\label{fig:hardwareext}\includegraphics[height=6cm,width=0.32\textwidth]{figures/hardware_extensions.pdf}}
%  \end{tabular}
%  \vspace{-5pt}
%  \caption{The figure provides an overview of the three solutions we
%    propose to track dirty data in persistent memory systems. (a) The
%    first approach uses a translation layer like Quill. (b) The second
%    approach uses compiler extensions. (c) The third approach uses
%    hardware extensions.}
%\label{fig:research}
%\end{figure*}

\vheading{Thrust 1: Tracking Dirty-Data via the POSIX
  Interface}. Persistent memory file systems such as ext4 have moved
away from the traditional POSIX read/write
interface~\cite{DBLP:journals/computer/Isaak90a,
  DBLP:conf/eurosys/AtlidakisAGMN16} due to the large overhead of
using system calls to access persistent
memory~\cite{linux-dax}. However, by using \mmap, they lose the
ability to track dirty data. We build on prior work termed
Quill~\cite{eisner2013quill}, a user-space library that interposes
between the application and the file system and translates POSIX
read/write system calls into loads and stores on the underlying
PM. While Quill provides such translation (thereby eliminating the
system call overhead), it does not take advantage of the extra
information gained from using a read/write interface: knowledge of the
exact bytes being updated. We propose to modify Quill to add such
fine-grained tracking. Our modified system will track dirty data on
every \syswrite system call; upon a subsequent \fsync, our system will
issue cache-line flushes only for dirtied data. Thus, our system will
significantly reduce \fsync overhead.

\vheading{Thrust 2: Compiler-Assisted Dirty-Data Tracking}. Thrust 1
is only applicable to applications that are using the POSIX read/write
interface. Newer applications that are written for persistent memory
also \mmap with direct loads and stores to the persistent memory. Our
second thrust is meant to help these applications. The basic idea is
to extend the compiler to track the persistent-memory writes of the
application. Since tracking all writes may be expensive, we may limit
the tracked region to a portion of the address space. The compiler
extension will then replace all writes to this region with two
instructions: the original write, and a second write to track the
application write. Thus, on \msync, flushes will be issued only for
the modified cache lines. The drawback of this approach will be the
cost of adding instructions to track the application writes.

}


