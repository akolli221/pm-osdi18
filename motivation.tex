\section{Background and Motivation}
\label{sec-bkgd}

We first provide some background on persistent memory systems and our assumptions about them (\sref{sec-pm}).
We then describe the two main challenges legacy applications face in fully leveraging the benefits of these systems (\sref{sec-legacy}).
Finally, we describe prior work on tackling these challenges and its deficiencies (\sref{sec-quill}).

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{Figures/persistent_memory_system.pdf}%
\caption{\textbf{Persistent Memory System:} We expect PMs to be
be placed alongside DRAM below the shared last level hardware cache.
Software can access the contents of the PM directly using processor
load/store instructions.
\ak{need better figure, that clearly shows the persistence domain.}}
\label{fig-pmsys}
\end{center}
\end{figure}



\subsection{Persistent Memory Systems}
\label{sec-pm}
PM devices are imminent. 
Intel and Micron's 3D XPoint~\cite{3dxpoint} is closest to commercialization while other contenders like phase change memory~\cite{DBLP:journals/micro/LeeZYZZIMB10, wong2010phase, lee2009pcm, raoux2008phase}, spin-torque transfer RAM~\cite{kultursay2013evaluating}, and memristors~\cite{strukov2008missing, chua2011resistance} are also being developed in both academia and industry.
These technologies are all non-volatile (can retain data across power failures), byte-addressable, and exhibit access latencies close to that of Dynamic Random Access Memory (DRAM).
It is widely expected~\cite{condit2009bpfs, xuswanson16, pelleychen14, arulrajperron16} that future PM systems will place PMs alongside DRAM on the memory bus, as shown in Figure~\ref{fig-pmsys}.
So, software can access PM contents directly using processor load/store instructions.
Moreover, since these technologies are non-volatile, they can be used for storage.

Since these technolgies are not yet commercially available, we make some (educated) assumptions about their characteristics.

\textbf{Persistent domain:} 
As per Intel's guidelines~\cite{intel2016pcommit,intel_adr}, we expect the PM's memory controller to be part of the persistent domain.
So, the applications only have to writeback data from the processor caches to the memory controller (for example, using \clflush, \clflushopt, or \clwb instructions on
x86 systems~\cite{intelisa}).
So, a cacheline worth of data can be made persistent in as low as 40ns (\clwb latency~\cite{xuswanson16}).

\textbf{Access latencies:}
While PMs are faster than conventional storage, they will be slower than DRAM.
PMs are expected to have access latencies 2-5\myx slower than DRAM and bandwidth 8\myx lower than DRAM~\cite{KwonFHPWA17,nvmdb2015suzuki,xuswanson16}.
We will factor in the performance characteristics of PMs in our evaluation (\sref{sec-eval}).

\textbf{Memory page size:}
These techonologies are much denser than DRAM~\cite{3dxpoint} and PMs will be deployed in TBs.
For example, Intel's 3D XPoint is 10\myx more dense than DRAM~\cite{3dxpoint} and SAP HANA~\cite{sappm} is preparing for servers with 6 TB of first generation 3D XPoint.
Given the size of PMs and the increasing trend of using huge pages (2MB or higher)~\cite{kwon16ingens}, we anticipate future PM systems to predominantly use huge pages.
So, we assume memory page sizes of 2MB in this work.


Given these characteristics of PM systems, we next list the challenges they pose to POSIX-based applications.


\subsection{Challenges for POSIX applications}
\label{sec-legacy}

%\vc{It would be great if we showed some actual numbers here. The 2x
%  for system call/library call overhead, the 3x in
%  non-temporal/temporal writes should be brought out here}

Most file systems and applications running on top of them are written assuming a slow, block storage device.
With with advent of PM systems, application developers can completely redesign their applications for PMs, an approch being widely considered in academia~\cite{pmkvstores, databases, etc.}.
However, we expect a vast majority of existing and future applications to continue to rely on the tried and tested POSIX interface.
With such applictions in mind, system designers have proposed file systems like ext4DAX~\cite{linux-dax}, Windows DAX~\cite{windowsdax}, NOVA~\cite{xuswanson16}, etc., optimized for PMs while still providing a POSIX interface to applications.
While these file systems are great first steps, we see two drawbacks in these implementations:

\textbf{High system call overheads:} Even with PM-optimized file systems, applications have to use system calls to read from and write to storage. 
With PM systems exhibiting drastically lower access latencies than traditional media (tens of nano seconds vs tens of micro seconds), the system call overhead from context switching between the application and the kernel becomes a major performance bottleneck.
For example, on a 4KB \sysread operation, \todo{xx\% of the operation latency is due to system call overhead on SSD-based systems and that fraction rises to xx\% in PM systems.}
We believe the high system call overhead suffered with ext4DAX and NOVA to be a major performance bottleneck and addressing this bottleneck is the primary focus of our work.

\textbf{High PM access latenies:} Both ext4DAX and NOVA use non-temporal store instructions to write to PM (\eg \movnti~\cite{intel_isa}).
These instructions bypass the cache hierarchy and write directly to the memory controller.
While these instructions are great for applications that exhibit little to no locality they are not good for applications with decent locality (temporal or spatial).
Fr such applications, it would be beneficial to use regular, temporal store instructions that leverage the processor cache hierarchy.
However, the downside of using temporal store instructions is that the latest versions of data could be buffered in the cache hierarchy and needs to be explicitly written back to the memory controller (using a \writeback instruction like \clflush or \clflushopt or \clwb) to ensure persistence.

In this work, we tackle both of these bottlenecks to help POSIX applications reap the full benefits of PM systems.


%\textbf{Writeback amplification:} These file systems rely on the virtual memory subsystem to track dirty data and then writeback the dirty data (for example, upon an \fsync) to persistence using writeback instructions (\eg \clflush on x86 machines).
%With this approach, even a small change to a memory page (\eg 1 byte in a 2MB page) causes the entire page to be considered ``dirty'' and results in the entire page being written back, leading up to 32768\myx amplification in the number of writeback instructions being issued. 

%Both of these challenges need to be addressed to maximize the performance 
%of POSIX-based applications on future PM systems.
%Next, we describe Quill, prior work that aims to tackle these challenges.

\subsection{Relevant prior work - Quill}
\label{sec-quill}

\begin{figure}
\centering
\includegraphics[width=4cm, height=5cm]{Figures/quill_api.pdf}%
\caption{\textbf{Quill's software architecture:} Quill is a user-space
library that sits between the application and a DAX file system.
Quill maintains \mmaped regions of files, later intercepts file
system calls made by the application and converts them to \memcpy
operations on the corresponding \mmaped regions.}
\label{fig-quill}
\end{figure}

Quill~\cite{eisner2013quill} is a user-space library with similar goals as our work. 
As shown in Figure~\ref{fig-quill}, Quill interposes between the file system and the application.
Quill \mmap-s the file being accessed (transparently to the application) and subsequently intercepts all \sysread, \syswrite, and \fsync system calls from the application and issues corresponding \emph{temporal} \memcpy calls and \writeback instructions to the underlying file system.
Quill relies on a DAX file system underneath~\cite{dax} to directly map PM pages into application's virtual address space and not incur unnecessary page copies from DRAM to PM when persisting \mmaped regions.
Effectively, Quill intercepts system calls from the application and translates them to user-space library calls, significantly reducing system-call overheads for some applicaations.
However, we identified two major deficiencies in the implementation of Quill that limit its effectiveness.

\begin{table}[t!]
\centering
\begin{tabular}{|l|l|}
    \hline
    {\bf Workloads} & {\bf \% of POSIX calls}\\
    \hline
    Web-server & \todo{0.45} \\
    File-server & \todo{0.64} \\
    Mail-server & \todo{0.38} \\
    TPCC & \todo{0.74} \\
    \hline
\end{tabular}
\caption{Fraction of application POSIC system calls taht are
not handled by Quill and instead are simply passed through to
the underlying file system. \ak{\bf FAKE numbers!}}
\label{tab:fraction}
\end{table}

\paragraph{Insufficient system call handling:} 
While Quill hopes to convert all file system calls into library calls, it falls quite short of this goal. 
Quill (conservatively) only mmaps certain portions of a file, not the entire file.
Quill views files as 512MB increments and only if the file size grows beyond a multiple of 512MB does it mmap the prior 512MB region.
For example, for a 600MB file, 0-512MB maybe \mmaped while 512-600MB may not be \mmaped and for a 1200MB file, 0-512MB and 512-1024MB may be \mmaped while 1024-1200MB may not be mmapped.
As a consequence, any system calls issued to non-\mmaped portions of the file, either due to file over-writes or file reads or file appends are simply passed on to the underlying file system as system calls.
This approach leads to high system call overheads, especially for append-heavy workloads or workloads dealing with small files.
Table~\ref{tab:fraction} shows the fraction of all POSIX system calls not handled by Quill and simply passed through to the underlying file system for different real world benchmarks (details in \sref{sec-methodology}).
For these workloads, Quill cannot handle anywhere from \todo{xx\% - xx\%} of the system calls issued by the applications.
Further more, Quill destroys memory mmapings for files as soon as they are closed, leading to high \mmap and \munmap overheads for files that are frequently opened and closed (\eg Mail-Server from Filebench~\cite{wilson2008new})

\paragraph{Writeback amplification:} 
Quill uses temporal store instructions to update \mmaped portions of files.
To ensure data persistence, Quill must writeback modified cachelines, for example on a subsqeuent \fsync.
However, our preliminary analysis on Quill showed that it does not implement \emph{any} dirty-data tracking mechanisms and writesback the entire mapping on an \fsync even if only a small portion of the mmaping has been modified.
Ideally, we would like to writeback only the cachelines that have been modified, all the other writeback operations issued to cachelines that have not been modified are unnecessary and incur a huge performance hit.
We refer to these unnecessary writeback instructions as \emph{writeback amplification}.
Writeback amplification is an especially accute problem for systems with huge memory pages (expected in future PM systems) where a single dirty bit is used to denote if any of the cachelines in the entire page are dirty.
For example, with 2MB pages, 1 dirty bit conveys the dirtiness of 32K 64B cachelines, leading up to 32768\myx writeback amplification in the worst case when only one cacheline in a 2MB page has been modified.
That kind of writeback amplifications can translate to execution time overheads of \todo{xx\myx}.
More over, as we show later (\sref{sec-eval}), writeback amplification can cause Quill to perform \emph{worse} that directly issuing file system calls to the underlying DAX file system.
%We developed a micro-benchmark, \filewb, that \mmap a 500MB file, reads a 100MB region of the file and then issues writes random cachelines to random cachelines in the 100MB region and finally issues an \fsync that writesback the dirty data using writeback instructions (\clflush, \clflushopt, or \clwb
%on x86).
%We observe that tracking dirty data at cacheline (64B) granularity and
%issuing only the necessary \clflushopt instructions causes \fsync to be
%\todo{XX}\myx faster than tracking dirty data at huge page (2MB) granularity. 

Next, we describe the design of \system, our system to effectively handle both high system call overhead and writeback amplification and help POSIX applications fully leverage the benefits of PM systems.

%Next, we describe the design principles for \system, our extensions to Quill
%to address the challenges described above.

%\vc{This is great, but this only talks about quill, which is one part
%  of the design space. I think we should also about PM file systems
%  like ext4 DAX, which use non-temporal writes, but at the cost of
%  system call latency}.

%\vc{We probably also want to explain what temporal and non-temporal
%  writes are}












