\section{Evaluation}
\label{sec-eval}

\subsection{Methodology}
\label{sec-methodology}

\subsection{Microbenchmarks}
\label{sec-microbench}

In this section, we use a suite of carefully designed microbenchmarks
to understand the trade-offs of using \system.
We designed these microbenchmarks to both show the costs associated with
using \system and also to highlight the potential benefits.
In particular, these are the following questions we wish to answer:
\begin{itemize}
\item Does converting system calls to library calls improve performance?
\item Does dirty data tracking improve performance?
\item What are the overheads associated with \system's \mmap/\munmap and other metadata operations?
\item What are the overheads of maintaining dirty data tracking metadata (\eg the DDTs)?
\item Does \system scale for multi-threaded applications?
\end{itemize}
Next, we try to answer each of the above questions using a few microbenchmarks
each.

\begin{figure*}[h!]
  \centering
  \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-random-reads.png}
  \caption{Random 4KB reads}
  \label{fig:randomreads}
  \end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-100appends-fsync.png}
  \caption{100 4KB appends + fsync}
  \label{fig:appendfsync}
  \end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-random-overwrite-fsync.png}
  \caption{Random 4KB overwrite + fsync}
  \label{fig:overwritefsync}
  \end{subfigure}
\caption{\textbf{Microbenchmark evaluation:} 
\ak{Need better figures here. Numbers are from the google doc. NOVA numbers extrapolated from what we have. Need to re-run them.}}
\label{fig:microbenchmark1}
\end{figure*}

\begin{figure*}[h!]
  \centering
  \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-random-overwrite-dirty-tracking.png}
  \caption{Random 4KB overwrite}
  \label{fig:randomowdirty}
  \end{subfigure}
  \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-random-overwrite-fsync-dirty-tracking.png}
  \caption{Random 4KB overwrite + fsync}
  \label{fig:randomowfsdirty}
  \end{subfigure}
  \begin{subfigure}[t]{0.32\textwidth}
  \centering
  \includegraphics[width=\textwidth]{Figures/eval-open-close.png}
  \caption{Create + Open + Close + Delete}
  \label{fig:openclose}
  \end{subfigure}
\caption{\textbf{Microbenchmark evaluation:} 
\ak{Need better figures here. Numbers are from the google doc as much as possible, other are fake that I made up.}}
\label{fig:microbenchmark2}
\end{figure*}


\paragraph{Does converting system calls to library calls improve performance?}
To understand the benefits of converting file system calls into \system library calls, we study the performance of a benchmark that can be serviced by \system without making any system calls.
We compare the performance of this microbenchmark on PM file systems (ext4DAX and NOVA) and on \system.
The performance improvement seen with \system can be directly traced back to converting system calls to library calls.

\textit{Microbenchmark-1: Random Reads} -- This workload randomly reads 4KB regions in a 5GB file.
We issue a total of 1M such read operations.
As can be seen from Figure~\ref{fig:randomreads} both Quill and \system, which aim to minimize system call overhead, cut the execution time to almost half compared to ext4DAX and NOVA, file systems which are optimized for PM.
This result highlights the performance gains possible with this approach.

\textit{Microbenchmark-2: Appends + \fsync} -- This workload appends 4KB to a file for 100 times and then issues an \fsync.
This pattern is repeated for 10000 times.
ext4DAX, NOVA, and Quill all issue a file system call for every append and \fsync.
However, \system buffers up all the 100 appends per iteration in a DRAM buffer and then issues one synchronous write system call on an \fsync, reducing the number of system calls.
As can be seen from Figure~\ref{fig:appendsfsync}, \system cuts the execution time by almost 4\myx.
Append system calls are expensive as they not only incur the overheads associated with all system calls (\eg context switch), they also have to extend the file, allocate new space for the file in PM, etc.
So, \system's approach of reducing file append system calls can significantly improve performance for appen-heavy workloads.
One caveat here is that if the application frequently issues \fsync between appends, that reduces the opportunity to buffer and coalesce append systems limiting the performance of \system.
For example, if an \fsync is issued after every append, \system performs comparably to ext4DAX.

\paragraph{Does dirty data tracking improve performance?}
Next, we use a different microbenchmark to understand how beneficial accurate dirty data tracking can be.

\textit{Microbenchmark-3: Random overwrite + \fsync} -- This workload overwrites a randomly chosen 4KB region in a 5GB file and immediately issues an \fsync.
This pattern is repeated for a total of 1M times.
As can be seen from Figure~\ref{fig:overwritefsync}, \system outperforms ext4DAX and NOVA by about 2\myx, these gains are pridominantly from converting file system calls into library calls.
Both ext4DAX and NOVA use non-temporal store instructions and do not suffer from writeback amplification.
However, Quill, which tries to reduce the system call overheads, uses temporal store instructions and does not implement any dirty data tracking, instead writing back all the \mmaped regions on an \fsync.
We extend Quill to track dirty-data at 2MB page granularity (by using the virtual memory system's dirty tracking).
Even with the extension, \system outperforms Quill by about 42\myx.
This microbenchmark highlights how important accurate dirty data tracking can be, however, the exact benefits depends on the access pattern exhibited by the application.

\paragraph{What are the overheads associated with \system's \mmap/\munmap and other metadata operations?}
Next, we look at the overheads from all the metadata operations to maintain the internal state at \system to help serve file system calls.

\textit{Microbenchmark-6: Create; Open; Close; Delete} -- This workload simply creates a file, opens it, then closes it and deletes it.
This pattern is repeated 1M times.
As shown in Figure~\ref{fig:openclose}, \system performs about 35\% worse than ext4DAX and NOVA while also being 35\% better than Quill.
\system's worse performance when compared to ext4DAX and NOVA is to be expected as all of the file system calls in this workload are passed through to the UFS while also updating its own internal state.
However, in real applications these kinds of calls are infrequent and these costs are offset by \system's better performance on the more frequent \sysread and \syswrite system calls.

\paragraph{What are the overheads of maintaining dirty data tracking metadata (the DDTs)?}
Next, we use a couple of microbenchmarks to show the overheads associated with accessing \system's dirty tracking metadata.

\textit{Microbenchmark-4: Random overwrites} -- This workload overwrites an randomly chosen 4KB region in a 5GB file.
1M such writes are performed. No \fsync is issued in this microbenchmark.
As shown in Figure~\ref{fig:randomowdirty}, \system with DDTs performs 35\% and 22\% worse than versions of \system that perform no dirty tracking and uses FDA respectively.
As expected \system with DDT performs worse with DDT as it incurs more metadata writes.
However, the payoff of this overhead comes on \fsync which we look at next.
\ak{Alert! Numbers made up.}

\textit{Microbenchmark-5: Random overwrite + \fsync} -- This workload overwrites an randomly chosen 4KB region in a 5GB file and immediately issues an \fsync.
This pattern is repeated for 1M times.
As shown in Figure~\ref{fig:randomowfsdirty}, \system with DDTs performs 47\myx and 2\myx better than versions of \system that perform no dirty tracking and uses FDA respectively.
The performance gains of \system with DDT over no dirty tracking comes mainly from mimimizing writeback amplification.
However, \system with DDT and FDA both issue the same number of \writeback instructions the performance difference between them stems only from how dirty tracking metadata is organized and accessed.
The efficient scan provided by DDT during an \fsync translates to over 2\myx improvement in performance.
\ak{Alert! Numbers made up.}

\begin{figure}
\begin{center}
  \includegraphics[scale=0.3]{Figures/eval-scaling.png}
  \caption{\textbf{\system scaling:} This figure shows how multi-threaded applications scale on \system.
  We show how two microbenchmarks (random reads and reandom overwrite + fsync) scale with increasing number of threads.
  We also present linear scaling for performance.
  \ak{FAKE NUMBERS!}}
  \label{fig:scaling}
\end{center}
\end{figure}


\paragraph{How do mulit-threaded applications scale on \system?}
As shown in Figure~\ref{fig:scaling} \todo{Need numbers and text here.}

After understanding all the costs and benefits of using \system and why different design choices were made, we next present \system's performance on real world benchmark.

\begin{figure}
\begin{center}
  \includegraphics[scale=0.3]{Figures/eval-real.png}
  \caption{\textbf{Real workload comparison:} This figure shows ext4DAX, NOVA, Quill, and \system perform
  on a suite of real world applications.
  \ak{FAKE NUMBERS!}}
  \label{fig:real}
\end{center}
\end{figure}

\subsection{Real world workloads}
\label{sec-workloads}

In this section, we compare the performance of \system with Quill, ext4DAX, and NOVA
for a set of real world workloads.
We use mail-server, file-server, and web-server from filebench, and TPC-C.

\ak{Looks like filebench has another workload, webproxy, we should consider adding that to our list.}

As shown in Figure~\ref{fig:real} \todo{Need numbers and text here.}